package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends ArrayAdapter<String> {
    AnimalList animal_list;

        public Adapter(Context context, String[] animals) {
            super(context, 0,  animals);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {


            // charger les nom et les image
            if (view == null) {

                view = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            }
            //recuperer le nom de hachmap
            String animalname =(String) getItem(position);


           // charger le nom  de l'animal
            final Animal animal = animal_list.getAnimal(animalname);
            TextView animalName = (TextView) view.findViewById(R.id.animal);
            animalName.setText(animalname);
            // charger l'amge a l'aide de getimage file
            ImageView image = (ImageView) view.findViewById(R.id.image);
            int id = view.getResources().getIdentifier("com.example.myapplication:drawable/" +animal.getImgFile() , null, null);
            image.setImageResource(id);



            return view;
        }


}
