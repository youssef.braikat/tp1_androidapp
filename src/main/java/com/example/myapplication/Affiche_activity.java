package com.example.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Affiche_activity extends AppCompatActivity {


    String animalSelectedName;
    Animal animalSelected;
    ImageView image;
    TextView animalName;
    TextView AhightestLifespan;;
    TextView AgestationPeriod;
    TextView AbirthWeight;
    TextView AadultWeight;
    EditText AconservationStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche);
        AnimalList listAnimal= new AnimalList();
        //Relie les variables et le View
        image = (ImageView) findViewById(R.id.imageView);
        animalName = (TextView) findViewById(R.id.animalName);
        AhightestLifespan = (TextView) findViewById(R.id.AhightestLifespan);
        AgestationPeriod= (TextView) findViewById(R.id. AgestationPeriod);
        AbirthWeight= (TextView) findViewById(R.id.AbirthWeight);
        AadultWeight = (TextView) findViewById(R.id.AadultWeight);
        AconservationStatus = (EditText) findViewById(R.id.AconservationStatus);

        //recuperation de l'animal selectionné
        Intent intent = getIntent();
        animalSelectedName = intent.getStringExtra("animal");
        animalSelected  = listAnimal.getAnimal(animalSelectedName);

        //recuperation l'image de l'animal
        int id = getResources().getIdentifier("com.example.myapplication:drawable/" + animalSelected.getImgFile().toString(), null, null);
        image.setImageResource(id);

        //recuperation des information de l'animal
         animalName.setText(animalSelectedName.toString());
        AhightestLifespan.setText(animalSelected.getStrHightestLifespan().toString());
        AgestationPeriod.setText(animalSelected.getStrGestationPeriod().toString());
        AbirthWeight.setText(animalSelected.getStrBirthWpréoccupation_mineureeight().toString());
        AadultWeight.setText(String.valueOf(animalSelected.getStrAdultWeight()));
        AconservationStatus.setText(String.valueOf(animalSelected.getConservationStatus()));

        //le botton d'enregistrement
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nouve= AconservationStatus.getText().toString();


                    if(!animalSelected.getConservationStatus().equals(nouve)){

                        animalSelected.setConservationStatus(nouve);

                        Toast.makeText(getApplicationContext(),"la modification a bien été effectuée ",Toast.LENGTH_LONG).show();
                    }

                finish();



            }
        });



}

}
